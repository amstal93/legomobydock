# Lego Moby Dock

A Lego replica of [Docker's logo](https://en.wikipedia.org/wiki/Docker_(software)) "Moby Dock".

## Story

It seems that Docker Inc. designed a Lego model of their logo, which they handed out to keynote attendees. Unfortunately, info about this is hard to find and the model itself is nowhere to be found. I've found a [single blog post about the docker Lego model](https://blog.loof.fr/2015/11/dockercon15-lego-moby-dock.html), but most of the images won't load on the site. There's also a [GitHub repository containing a file for the Lego Digital Designer](https://github.com/OscarBarrett/MobyDock-Lego), but this model seems to be impossible to build without glue (which, in my opinion, contradicts the basic idea of Lego).

Therefore I created a model from scratch which can be built with commonly available bricks. The MobyDock.io file can be opened with the [Studio 2.0 from BrickLink](https://www.bricklink.com/v2/build/studio.page), which also integrates a marketplace for Lego bricks. I've also included two renders and a PDF-file containing the instructions to build the model.

Have fun!

![Photoreal rendering of Moby Dock](MobyDock_photoreal.jpg)

